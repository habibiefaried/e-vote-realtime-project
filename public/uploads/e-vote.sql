-- MySQL dump 10.13  Distrib 5.5.35, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: e-vote
-- ------------------------------------------------------
-- Server version	5.5.35-0ubuntu0.13.10.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answer`
--

DROP TABLE IF EXISTS `answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_calon` varchar(100) NOT NULL,
  `deskripsi_calon` text NOT NULL,
  `foto_calon` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answer`
--

LOCK TABLES `answer` WRITE;
/*!40000 ALTER TABLE `answer` DISABLE KEYS */;
INSERT INTO `answer` VALUES (1,'test','test','hadoop_elephant.png'),(2,'iseng','<p>iseng banget parah</p>','Screenshot from 2014-02-11 19:21:37.png'),(3,'hanif','<p>ganteg</p>','Screenshot from 2014-02-12 21:16:53.png'),(4,'test','<p><strong>Lalal</strong></p>\r\n<p>Oke</p>','Screenshot from 2014-02-12 20:34:50.png');
/*!40000 ALTER TABLE `answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `print_queue`
--

DROP TABLE IF EXISTS `print_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `print_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_calon` int(11) NOT NULL,
  `id_pemilih` int(11) NOT NULL,
  `time_vote` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `print_queue`
--

LOCK TABLES `print_queue` WRITE;
/*!40000 ALTER TABLE `print_queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `print_queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `voters`
--

DROP TABLE IF EXISTS `voters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `KTP` varchar(100) NOT NULL,
  `Nama_lengkap` varchar(100) NOT NULL,
  `Alamat` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `voters`
--

LOCK TABLES `voters` WRITE;
/*!40000 ALTER TABLE `voters` DISABLE KEYS */;
INSERT INTO `voters` VALUES (1,'5412','habibie','habibie'),(2,'1298','Habbie','jl. test2'),(3,'31090','testing','testing'),(4,'1122','hanif','jl. apake'),(5,'123','123','1234'),(8,'12345','test','test'),(9,'8989','esdshg','ik'),(10,'89189','adasd','asdasda'),(11,'7841','testing','testing'),(12,'1234','1234','1234'),(13,'10912','123','1323'),(14,'8921','asdsa','asdsad'),(15,'9090','9090','9090'),(16,'1010','1010','1010'),(17,'9102','12','121'),(18,'1234567','1234123','123123'),(19,'6789','6789','6789'),(20,'123456789','Habibie Faried','Jl. Pelesiran'),(21,'6745','6745','6745'),(22,'456','123','123'),(23,'4523','123','23123'),(24,'451','451','451'),(25,'091','091','091');
/*!40000 ALTER TABLE `voters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `voters_answer`
--

DROP TABLE IF EXISTS `voters_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voters_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_voters` int(11) NOT NULL,
  `id_answer` int(11) NOT NULL,
  `time_vote` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `voters_answer`
--

LOCK TABLES `voters_answer` WRITE;
/*!40000 ALTER TABLE `voters_answer` DISABLE KEYS */;
INSERT INTO `voters_answer` VALUES (1,2,1,'2014-02-05 16:08:09'),(2,3,1,'2014-02-06 16:33:18'),(3,1,1,'2014-02-06 16:35:58'),(4,4,1,'2014-02-06 16:36:47'),(5,5,1,'2014-02-06 16:57:28'),(7,8,1,'2014-02-13 19:33:53'),(8,9,1,'2014-02-14 15:20:20'),(9,10,1,'2014-02-24 13:36:32'),(10,11,2,'2014-02-25 14:06:00'),(11,13,2,'2014-02-25 20:30:40'),(12,15,1,'2014-02-28 22:44:50'),(13,16,3,'2014-02-28 22:45:46'),(14,17,3,'2014-03-05 19:03:30'),(15,18,2,'2014-03-14 18:32:59'),(16,19,3,'2014-03-16 01:43:32'),(17,21,1,'2014-03-16 20:02:18'),(18,22,4,'2014-03-17 12:10:02'),(19,23,2,'2014-03-20 19:05:04'),(20,24,1,'2014-03-21 19:00:20'),(21,25,1,'2014-03-21 19:01:05');
/*!40000 ALTER TABLE `voters_answer` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-03-24 19:36:06
