var express = require('express')
  , vote = require('./routes/vote')
  , admin = require('./routes/admin')
  , config = require('./routes/config')
  , socketio = require('socket.io')
  , http = require('http')
  , path = require('path');

var app = express();
var jam; var menit; var detik;

// all environments
app.set('port', process.env.PORT || 8080);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.set('path_upload', __dirname+"/public/uploads/");
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('proyeke-votekerens'));
app.use(express.session());
app.use(app.router);
app.use(require('less-middleware')({ src: __dirname + '/public' }));
app.use(express.static(path.join(__dirname, 'public')));


localQuery = function(req, res, next) {
  res.locals.session = req.session;
  next();
};

app.get('/',localQuery, vote.index); //laman utama
app.post('/voteprocess',localQuery,vote.process); //menangani siapa yang dipilih

app.get('/admin',localQuery,admin.index); //laman index admin
app.get('/admin/login',localQuery,admin.login); //laman admin login
app.post('/admin/login',localQuery,admin.loginproses); //admin login proses
app.get('/admin/konsolbilik',localQuery,admin.konsolbilik); //laman admin untuk memulai bilik Online
app.get('/admin/calon',localQuery,admin.calon); //laman admin untuk memasukkan calon
app.get('/admin/calonadd',localQuery,admin.calonadd); //laman memasukkan calon baru
app.post('/admin/calonadd',localQuery,admin.calonproses); //admin untuk proses memasukkan calon kedalam database
app.get('/admin/calondel/:id',localQuery,admin.calondel); //admin untuk proses delete calon dari database
app.get('/admin/voters',localQuery,admin.voters); //lihat voters
app.post('/admin/addvoters',localQuery,admin.votersadd); //tambah voters
app.get('/admin/delvoters/:id',localQuery,admin.votersdel); //delete voters
app.post('/admin/editvoters/:id',localQuery,admin.votersedit); //edit voters
app.get('/admin/hasilpemilu',localQuery,admin.hasilpemilu); //lihat hasil pemilu saat ini
app.get('/admin/logout',localQuery,admin.logout);
app.post('/admin/isPemilihOK',localQuery,admin.isPemilihOK); //cek apakah pemilih OKE
app.get('/admin/print',localQuery,admin.print);
app.get('/admin/deletePrint/:id',localQuery,admin.deletePrint);
app.get('/admin/live_report',localQuery,admin.livereport);

app.get('/admin/timeconfig',localQuery,admin.timeconfig); //laman timeconfig
app.post('/admin/settime',localQuery,admin.timeset); //set timeconfig

app.get('/admin/getVoters',localQuery,admin.getVoters); //REST web untuk mengambil jumlah orang yang sudah ngevote
app.get('/admin/getDPT',localQuery,admin.getDPT); //REST web untuk mengambil jumlah orang yang melakukan voting
app.get('/admin/getPrint',localQuery,admin.getPrint); //REST web

app.post('/admin/print_pemilih',localQuery,admin.print_pemilih);
app.get('/admin/cekPassword',localQuery,admin.cekPassword);

//Database
app.get('/admin/cleardb',localQuery,admin.cleardb);
app.get('/admin/exportdb',localQuery,admin.exportdb);

/*============================================
Socket.io Server
============================================*/
var server = app.listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});

var io = socketio.listen(server);
//listening socket.io client in express port

var listAvailBilik = []; //disini list semua bilik available (Bilik-Nomor)
var listAllBilik = []; //(semua bilik)
var isAdminUP = false;
//io.set('log level', 3);
//Algoritma socket.io

setInterval(function() {
	if (config.isVotingSet && config.counter>0) config.counter--;
	io.sockets.emit("counter_waktu",config.counter);
},1 * 1000, 1000);

io.sockets.on('connection', function (socket) {
	socket.on('vote_id_voter',function(msg) {
		config.data.query("INSERT INTO voters_answer (id_voters,id_answer,time_vote) VALUES ('"+msg.id_vote+"','-2','"+config.getDateTime()+"');",function(err,results) {
			if (err) throw err;
		});	
	});

	socket.on('sedang_voting',function(data){
		io.sockets.emit("sedang_voting",data);
	});

	socket.on('done_voting',function(data){
		io.sockets.emit("done_voting",data);
	});

	socket.on('setOnBilik',function(data){
		console.log(data);
	});

	socket.on('bilikReg',function(data){
		if (isAdminUP) socket.emit('adminUP',{data:"ok",bilik:listAvailBilik});
		else socket.emit('adminUP',{data:"not ok"});	
	});
	
	socket.on('bilikActivate',function(data){
		var logs = "Bilik ke-"+(parseInt(data.msg)+1)+" sudah diaktivasi";
		console.log(logs);
		socket.join(listAvailBilik[data.msg]); //join room bilik
		console.log(listAvailBilik[data.msg]);
		socket.nomorBilik = data.msg; //disimpan

		delete listAvailBilik[data.msg];
		io.sockets.emit('adminUP',{data:"ok",bilik:listAvailBilik});
		io.sockets.in("AdminRoom").emit("Logging",{msg:logs,no_bilik:data.msg,status:"ON"});
	});

	socket.on('adminReg',function(data) {
		console.log("Server telah up!");
		socket.join("AdminRoom"); //room admin khusus
		socket.nomorBilik = -1; //disimpan

		var date = new Date();
		jam = date.getHours();
		menit = date.getMinutes();
		detik = date.getSeconds();
		
		console.log("Admin dinyalakan waktu "+jam+":"+menit+":"+detik);

		isAdminUP = true;
		var i=1;
		listAvailBilik = [];
		while (i<=config.jmlbilik){	
			listAvailBilik.push("Bilik-"+i);
			listAllBilik.push("Bilik-"+i);
			//socket.join("Bilik-"+i); //Join socket room
			i++;
		}
		io.sockets.emit("adminUP",{data:"ok",bilik:listAvailBilik});
	});

	socket.on('disconnect', function(){
		if (socket.nomorBilik == -1) console.log("ERROR!, laman admin terputus. Silahkan anda restart server e-vote ini");
		else {
			var logs = "WARN, bilik-"+(parseInt(socket.nomorBilik)+1)+" terputus";
			console.log(logs);
		   io.sockets.in("AdminRoom").emit("Logging",{msg:logs,no_bilik:socket.nomorBilik,status:"OFF"});
			io.sockets.in("AdminRoom").emit("statusVote",{msg:"done",no_bilik:socket.nomorBilik});
			listAvailBilik[socket.nomorBilik] = "Bilik-"+(parseInt(socket.nomorBilik)+1);
		}
	});

	socket.on('setOnBilik',function(data){
		io.sockets.in(data.bilik).emit("startVote",data);
	});

	socket.on('onVoteProcess',function(data){
		io.sockets.in("AdminRoom").emit("statusVote",data);
	});
});
