-- phpMyAdmin SQL Dump
-- version 4.0.6deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 02, 2014 at 07:03 AM
-- Server version: 5.5.35-0ubuntu0.13.10.2
-- PHP Version: 5.5.3-1ubuntu2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `e-vote`
--

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE IF NOT EXISTS `answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_calon` varchar(100) NOT NULL,
  `deskripsi_calon` text NOT NULL,
  `foto_calon` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `answer`
--

INSERT INTO `answer` (`id`, `nama_calon`, `deskripsi_calon`, `foto_calon`) VALUES
(1, 'test', 'test', 'hadoop_elephant.png'),
(2, 'iseng', '<p>iseng banget parah</p>', 'Screenshot from 2014-02-11 19:21:37.png'),
(3, 'hanif', '<p>ganteg</p>', 'Screenshot from 2014-02-12 21:16:53.png'),
(4, 'test', '<p><strong>Lalal</strong></p>\r\n<p>Oke</p>', 'Screenshot from 2014-02-12 20:34:50.png');

-- --------------------------------------------------------

--
-- Table structure for table `print_queue`
--

CREATE TABLE IF NOT EXISTS `print_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_calon` int(11) NOT NULL,
  `id_pemilih` int(11) NOT NULL,
  `time_vote` text NOT NULL,
  `is_print` tinyint(4) NOT NULL,
  `random_string` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `print_queue`
--

INSERT INTO `print_queue` (`id`, `id_calon`, `id_pemilih`, `time_vote`, `is_print`, `random_string`) VALUES
(2, 1, 1, '2014-03-27 03:13:18', 1, ''),
(3, 1, 1, '2014-03-31 17:52:52', 1, ''),
(4, 4, 28, '2014-04-02 13:59:35', 1, 'ycLzan');

-- --------------------------------------------------------

--
-- Table structure for table `voters`
--

CREATE TABLE IF NOT EXISTS `voters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `KTP` varchar(100) NOT NULL,
  `Nama_lengkap` varchar(100) NOT NULL,
  `Alamat` text NOT NULL,
  `desa` varchar(100) NOT NULL,
  `kecamatan` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `voters`
--

INSERT INTO `voters` (`id`, `KTP`, `Nama_lengkap`, `Alamat`, `desa`, `kecamatan`) VALUES
(1, '5412', 'habibie', 'habibie', '', ''),
(2, '1298', 'Habbie', 'jl. test2', '', ''),
(3, '31090', 'testing', 'testing', '', ''),
(4, '1122', 'hanif', 'jl. apake', '', ''),
(5, '123', '1234', 'behavioural', '', ''),
(8, '12345', 'test', 'test', '', ''),
(9, '8989', 'esdshg', 'ik', '', ''),
(10, '89189', 'adasd', 'asdasda', '', ''),
(11, '7841', 'testing', 'testing', '', ''),
(12, '1234', '1234', '1234', '', ''),
(13, '10912', '123', '1323', '', ''),
(15, '9090', '9090', '9090', '', ''),
(16, '1010', '1010', '1010', '', ''),
(18, '1234567', '1234123', '123123', '', ''),
(19, '6789', '6789', '6789', '', ''),
(20, '123456789', 'Habibie Faried', 'Jl. Pelesiran', '', ''),
(22, '456', '123', '123', '', ''),
(23, '4523', '123', '23123', '', ''),
(24, '451', '451', '451', '', ''),
(25, '091', '091', '091', 'testing', 'testing'),
(26, '671', '671', '671', '', ''),
(27, '891', '891', '891', '', ''),
(28, '78564', 'Habibie Faried', 'Jl. Pelesiran', 'Taman Sari2', 'Sari Asih');

-- --------------------------------------------------------

--
-- Table structure for table `voters_answer`
--

CREATE TABLE IF NOT EXISTS `voters_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_voters` int(11) NOT NULL,
  `id_answer` int(11) NOT NULL,
  `time_vote` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `voters_answer`
--

INSERT INTO `voters_answer` (`id`, `id_voters`, `id_answer`, `time_vote`) VALUES
(1, 2, 1, '2014-02-05 16:08:09'),
(2, 3, 1, '2014-02-06 16:33:18'),
(3, 1, 1, '2014-02-06 16:35:58'),
(4, 4, 1, '2014-02-06 16:36:47'),
(5, 5, 1, '2014-02-06 16:57:28'),
(7, 8, 1, '2014-02-13 19:33:53'),
(8, 9, 1, '2014-02-14 15:20:20'),
(9, 10, 1, '2014-02-24 13:36:32'),
(10, 11, 2, '2014-02-25 14:06:00'),
(11, 13, 2, '2014-02-25 20:30:40'),
(12, 15, 1, '2014-02-28 22:44:50'),
(13, 16, 3, '2014-02-28 22:45:46'),
(15, 18, 2, '2014-03-14 18:32:59'),
(16, 19, 3, '2014-03-16 01:43:32'),
(18, 22, 4, '2014-03-17 12:10:02'),
(19, 23, 2, '2014-03-20 19:05:04'),
(20, 24, 1, '2014-03-21 19:00:20'),
(21, 25, 1, '2014-03-21 19:01:05'),
(22, 26, 3, '2014-03-24 19:39:44'),
(23, 27, 1, '2014-03-24 19:41:36'),
(24, 12, 2, '2014-03-28 17:47:11'),
(25, 20, 2, '2014-03-31 17:41:38'),
(26, 28, 4, '2014-04-02 13:59:35');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
