var config = require("./config");

exports.index = function (req,res) {
	if (config.isVoteStopped) res.send("Proses voting sudah selesai");
	else if (!config.isVotingSet) res.send("Durasi voting belum diset");
	else{
		config.data.query("select * from answer",function(err,results){
			if (err) throw err;	
			else res.render('voter/index',{data:results,judul:config.nama_pemilihan});
		});
	}
};

exports.process = function (req,res) {
	/* 1. Jika ID_Voter tidak ada di tabel voter (tidak masuk daftar pemilih) -> keluar error (res.send("not ok"))*/
	/* 2. Jika ID_Voter sudah ada di tabel voters_answer (berarti sudah pernah milih) -> keluar error */
	/* Jika kedua kondisi diatas ok, masukkan ke database dan tinggal res.send("ok") */
	if (config.isVoteStopped) res.send("Proses voting sudah selesai");
	else if (!config.isVotingSet) res.send("Durasi voting belum diset");
	else {	
	var id_voter = parseInt(req.param("id_voter")); //vulnerable (Sudah ditangani)
	var id_answer = parseInt(req.param("id_answer")); //vulnerable (Sudah ditangani)

	config.data.query("SELECT COUNT(*) as JmlVoter from voters where id="+id_voter,function(err,results){
		if (err) throw err;
		else {
			if (results[0].JmlVoter == "0") { res.send("not_ok1"); console.log("ERROR: not_ok1"); }
			else {
				config.data.query("SELECT id_answer, COUNT(*) as JmlAns from voters_answer where id_voters="+id_voter,function(err2,results2) {
					if ((results2[0].JmlAns == "0") || (results2[0].id_answer==-2)) {
						config.data.query("UPDATE voters_answer SET id_answer="+id_answer+", time_vote='"+config.getDateTime()+"' WHERE id_voters="+id_voter, function (err3,results3)
						{
							if (err3) throw err3;
							else {
								var random_string = config.random_str(6);
								config.data.query("INSERT INTO print_queue (id_calon, id_pemilih, time_vote,random_string) VALUES ('"+id_answer+"','"+id_voter+"','"+config.getDateTime()+"','"+random_string+"');",function(err4,results4) {
								if (err4) throw err4;
								else
								res.send("ok"); //berhasil
							});
							}
						});
					}
					else { res.send("not_ok2"); console.log("ERROR: not_ok2"); }//sudah milih (kena kondisi 2)
				});
				}
			}
	});
	}
};
