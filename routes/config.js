/*===================================================================================================*/
/* Editable Configuration */
/*===================================================================================================*/

var user_db = "root"; //username database, silahkan diganti sesuai kondisi
var pass_db = "tkislam123"; //password database, silahkan diganti sesuai kondisi
var name_db = "e-vote"; //nama database yang akan digunakan, silahkan diganti sesuai kondisi 
exports.useradmin = "b14358601bf8ae87d5b42bb869c553b3";  //username dalam md5, JANGAN diganti apabila anda tidak mengerti
exports.userpass = "ca9902f2dcf8a5d63ee5b87a57580d9a"; //password dalam md5, JANGAN diganti apabila anda tidak mengerti
exports.jmlbilik = 5; //jumlah bilik yang disediakan, silahkan diganti sesuai kondisi

/*===================================================================================================*/
/* End Of Configuration */
/*===================================================================================================*/

var mysql = require('mysql');
var crypto = require('crypto');

var client = mysql.createConnection({user:user_db,password:pass_db,database:name_db});
exports.data = client;
exports.mysql = mysql;
 
function random (howMany, chars) {
    chars = chars
        || "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    var rnd = crypto.randomBytes(howMany)
        , value = new Array(howMany)
        , len = chars.length;
 
    for (var i = 0; i < howMany; i++) {
        value[i] = chars[rnd[i] % len]
    };
 
    return value.join('');
}

exports.isVoteStopped = false;
exports.isVotingSet = false; //awal2 false
exports.lamavoting;
exports.counter;

exports.nama_pemilihan;
exports.lokasi;
exports.password;

exports.user_db = user_db;
exports.pass_db = pass_db;
exports.name_db = name_db;
exports.random_str = random;

exports.getDateTime = function getDateTime() {
    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;

}
