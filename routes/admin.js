var config = require("./config");
var fs = require("fs");
var md5 = require("MD5");
var sys = require('sys')
var exec = require('child_process').exec;
var child;

exports.cleardb = function (req,res) {
	if (req.session.isLogin) {
	config.data.query("DELETE FROM answer",function (err, results) {
		if (err) throw err;
		else {
			config.data.query("DELETE FROM print_queue",function(err2,results2) {
			if (err2) throw err2;
			else {
				config.data.query("DELETE FROM voters",function(err3,results3) {
					if (err3) throw err3;
					else {
					config.data.query("DELETE FROM voters_answer",function(err4,results4) {
						if (err4) throw err4;
						else res.send("Seluruh data telah dihapus");
					});
					}
				});
			}
			});			
		}
	});
	}else res.redirect('admin/login');
};

exports.exportdb = function (req,res) {
	if (req.session.isLogin) {
	child = exec("mysqldump -u "+config.user_db+" -p"+config.pass_db+" "+config.name_db+" > "+req.app.get("path_upload")+config.name_db+".sql", function (error, stdout, stderr) {
	  if (error !== null) {
	    res.send('exec error: ' + error);
	  }else    res.redirect("/uploads/"+config.name_db+".sql");
	});
	}else res.redirect('admin/login');
};

exports.print_pemilih = function(req,res) {
	if (req.session.isLogin) {
	var query;
	if (req.param('jenis_pemilih') == "all")
		query = "SELECT * FROM voters";
	else
		query = "SELECT *, COUNT(voters_answer.id) as isVote from voters JOIN voters_answer ON voters.id=voters_answer.id_voters group by voters.id";

	config.data.query(query,function(err,results) {
		if (err) throw err;
		else {
			res.render('admin/print_pemilih',{data:results,nama_pemilihan:config.nama_pemilihan,lokasi:config.lokasi});
		}
	});
	}else res.redirect('admin/login');
};

exports.cekPassword = function(req,res) {
	if (req.param('password') == config.password) res.send("OK");
	else res.send("FAIL");
};

exports.timeconfig = function (req,res) {
	if (req.session.isLogin) {
		if (!config.isVotingSet) res.render('admin/timeconfig');
		else res.redirect('admin');
	}
	else res.redirect('admin/login');
};

exports.timeset = function (req,res) {
	if (req.session.isLogin) {
		config.isVotingSet = true;
		config.isVoteStopped = false;
		config.lamavoting = req.param('duration') * 3600;
		config.counter = req.param('duration') * 3600;
		config.nama_pemilihan = req.param('nama_pemilihan'); console.log(config.nama_pemilihan);
		config.lokasi = req.param('lokasi'); console.log(config.lokasi);
		config.password = req.param('password');
		setInterval(function(){
	  		console.log("Proses voting selesai");
	  		config.isVoteStopped = true;
		}, config.lamavoting * 1000, 1000);
		res.redirect('admin');
	} else res.redirect('admin/login');
};

exports.index = function (req,res) {
	if (req.session.isLogin) {
		res.render('admin',{data:config.lamavoting});
	}
	else res.redirect('admin/login');
};

exports.getVoters = function (req,res) {
	config.data.query("select count(*) as jmlVote from voters_answer",function(err,results){
		if (err) throw err;
		else {
			res.send(results[0]['jmlVote'].toString());
		}
	});
};

exports.getDPT = function (req,res) {
	config.data.query("select count(*) as jmlDPT from voters",function(err,results){
		if (err) throw err;
		else {
			res.send(results[0]['jmlDPT'].toString());
		}
	});
};

exports.getPrint = function (req,res) {
	//ganti (UDAH)
	config.data.query("select count(*) as jmlPrint from print_queue where is_print=0", function (err,results) {
		if (err) throw err;
		else res.send(results[0]['jmlPrint'].toString());
	});
};

exports.livereport = function(req,res){
	if (req.session.isLogin) {
		res.render('admin/live_report');
	}
	else res.redirect('admin/login');
};

exports.print = function (req,res){
	//select * from print_queue join answer where print_queue.id_calon=answer.id LIMIT 0,1;
	//ganti (UDAH)
	if (req.session.isLogin) {
		config.data.query("select * from answer join print_queue where print_queue.id_calon=answer.id AND is_print=0 LIMIT 0,1",function(err,results){
			if (err) throw err;
			else res.render('admin/print',{data:results});
		});
	}
	else res.redirect("admin/login");
};

exports.deletePrint = function (req,res) {
	if (req.session.isLogin) {
	//req.param('id') vulnerable (Sudah ditangani)
		config.data.query("update print_queue set is_print=1 where id="+parseInt(req.param('id')),function(err,results) {
			if (err) throw err;
			else res.redirect('admin/print');
		});
	}
	else res.redirect('admin/login');
};

exports.login = function (req,res) {
	res.render('admin/login');
};

exports.loginproses = function (req,res) {
	var username = md5(req.param('username'));
	var password = md5(req.param('password'));
	if ((username == config.useradmin) && (password == config.userpass)) {
		req.session.isLogin = true;
		req.session.mode = req.param('mode');
		res.locals.session = req.session;
		if (req.param('mode') == "pra") res.redirect('admin');
		else if (req.param('mode') == "non-pra") res.redirect('admin/timeconfig');
		else res.redirect('admin/login');
	}
	else res.redirect('admin/login');
};

exports.konsolbilik = function (req,res) {
	if (req.session.isLogin) {
		res.render('admin/bilik',{jmlbilik:config.jmlbilik});
	} else res.redirect('admin/login');
};

exports.calon = function (req,res) {
	if (req.session.isLogin) {	
		config.data.query("select * from answer", function(err,results) {
			if (err) throw err;
			else
				res.render('admin/calon',{data:results});
		});
	}else res.redirect('admin/login');
};

exports.calonadd = function (req,res) {
	if (req.session.isLogin) {
		res.render('admin/calonadd');
	}else res.redirect('admin/login');
};

exports.calonproses = function (req,res) {
	//proses calon add
	if (req.session.isLogin) {
		var nama_calon = config.mysql.escape(req.param("nama_lengkap")); //vulnerable tolerant (UDAH)
		var deskripsi_calon = config.mysql.escape(req.param("deskripsi_calon")); //vulnerable tolerant (UDAH)

		fs.readFile(req.files.foto_calon.path, function (err, data) {
		  var newPath = req.app.get("path_upload")+req.files.foto_calon.name;
		  console.log(newPath);
		  fs.writeFile(newPath, data, function (err) {
			 //proses memasukkan data ke database
			 config.data.query("insert into answer (nama_calon, deskripsi_calon,foto_calon) values ("+nama_calon+","+deskripsi_calon+",'"+req.files.foto_calon.name+"');",function (err2,results)
			 {
				if (err2) throw err2;
				else res.redirect('admin/calon');
			 });
		  });
		});
	}
	else res.redirect('admin/login');
};

exports.calondel = function (req,res) {
	if (req.session.isLogin) {
		var id = parseInt(req.param("id")); //Sudah ditangani
		config.data.query("delete from answer where id="+id,function(err,results) {
			if (err) throw err;
			else res.redirect('admin/calon');		
		});
	}else res.redirect('admin/login');
};

exports.voters = function (req,res) {
//UDAH BENER
//SELECT *, COUNT(voters_answer.id) from voters LEFT JOIN voters_answer ON voters.id=voters_answer.id_voters group by voters.id
//SELECT *, COUNT(voters_answer.id) as isVote from voters LEFT JOIN voters_answer ON voters.id=voters_answer.id_voters group by voters.id
	if (req.session.isLogin) {
		config.data.query("SELECT *, COUNT(voters_answer.id) as isVote, voters.id as id_vote from voters LEFT JOIN voters_answer ON voters.id=voters_answer.id_voters group by voters.id",function (err,results) {
			if (err) throw err;
			else res.render('admin/pemilih',{data:results});		
		});
	} else res.redirect('admin/login');
};

exports.votersadd = function (req,res) {
	if (req.session.isLogin) {
		var KTP = config.mysql.escape(req.param("KTP")); //vulnerable (Sudah ditangani)
		var Nama_lengkap = config.mysql.escape(req.param("nama_lengkap")); //vulnerable (sudah ditangani)
		var Alamat = config.mysql.escape(req.param("alamat")); //vulnerable (sudah)
		var Kec = config.mysql.escape(req.param("kecamatan")); //vulnerable (sudah)
		var Desa = config.mysql.escape(req.param("desa")); //vulnerable (sudah)
		
		config.data.query("SELECT COUNT(*) as jml FROM voters where KTP="+KTP, function (err,results) {
			if (err) throw err;
			else {
				if (results[0].jml == "0") {
					var query = "insert into voters (KTP,Nama_lengkap,Alamat,desa,kecamatan) values ("+KTP+","+Nama_lengkap+","+Alamat+","+Kec+","+Desa+");";
					console.log(query);
					config.data.query(query, function (err2,results) {
						if (err2) throw err2;
						else res.redirect('admin/voters');	
					});
				} else res.send("Error, Nomor KTP ini sudah ada pada daftar pemilih");
			}
		});
	}else res.redirect('admin/login');
};

exports.votersdel = function (req,res) {
	if (req.session.isLogin) {
		//req.param("id") vulnerable (UDAH)
		config.data.query("delete from voters where id = "+parseInt(req.param("id")),function(err,results) {
			if (err) throw err;
			else {
				//req.param("id") vulnerable (UDAH)
				config.data.query("delete from voters_answer where id_voters = "+parseInt(req.param("id")),function(err2,results) {				
					if (err2) throw err2;
					else				
						res.redirect('admin/voters');
				});
			}
		});
	}else res.redirect('admin/login');
};

exports.votersedit = function (req,res) {
	if (req.session.isLogin) {
		var id = parseInt(req.param("id")); //vulnerable (udah)
		var KTP = config.mysql.escape(req.body.KTP); //vulnerable (udah)
		var nama_lengkap = config.mysql.escape(req.body.nama_lengkap); //vulnerable (udah)
		var alamat = config.mysql.escape(req.body.alamat); //vulnerable (udah)

		var Kec = config.mysql.escape(req.param("kecamatan")); //vulnerable (udah)
		var Desa = config.mysql.escape(req.param("desa")); //vulnerable (udah)

		var query = "update voters set KTP="+KTP+", Nama_lengkap="+nama_lengkap+", Alamat = "+alamat+", desa = "+Desa+", kecamatan = "+Kec+" where id="+id;

		console.log(query);
		config.data.query(query,function(err,results) {
			if (err) throw err;
			else res.redirect('admin/voters');
		});
	}else res.redirect('admin/login');
};

exports.hasilpemilu = function (req,res) {
/* select answer.nama_calon, count(voters_answer.id_answer) as JmlVote from voters_answer join answer where voters_answer.id_answer = answer.id group by voters_answer.id_answer;

select * from voters_answer join answer where answer.id = voters_answer.id_answer (generate table)
*/
	if (req.session.isLogin) {
		config.data.query("select answer.nama_calon, count(voters_answer.id_answer) as JmlVote from voters_answer join answer where voters_answer.id_answer = answer.id group by voters_answer.id_answer",function(err,results) {
			if (err) throw err;
			else {
				config.data.query("select * from voters_answer join answer where answer.id = voters_answer.id_answer order by voters_answer.time_vote DESC",function(err2,results2) {
				if (err2) throw err2;
				else res.render('admin/hasil',{data:results,tabel:results2});
			});
			}		
		});
	} else res.redirect('admin/login');
};

exports.logout = function (req,res) {
	req.session.isLogin = false;
	res.redirect('admin/login');
};

exports.isPemilihOK = function (req,res) {
	/* 1. Jika ID_Voter tidak ada di tabel voter (tidak masuk daftar pemilih) -> keluar error (res.send("not ok"))*/
	/* 2. Jika ID_Voter sudah ada di tabel voters_answer (berarti sudah pernah milih) -> keluar error */
	/* Jika kedua kondisi diatas ok, tinggal res.send("ok") */
	//ERROR, 091 dianggap sama dengan 91
	var KTP = req.param("KTP");
	if (!isNaN(parseInt(KTP)))
	{
		KTP = parseInt(KTP); //vulnerable (UDAH)
		config.data.query("SELECT COUNT(*) as JmlVoter from voters where KTP="+KTP,function(err,results){
			if (err) throw err;
			else {
				if (results[0].JmlVoter == "0") res.json({msg:"not_ok1"}); //Kondisi 1
				else {
					config.data.query("SELECT COUNT(*) as JmlAns from voters_answer join voters where voters.id = voters_answer.id_voters and voters.KTP="+KTP,function(err2,results2) {
						if (err2) throw err2;

						if (results2[0].JmlAns == "0") {
							config.data.query("SELECT * FROM voters where KTP="+KTP,function(err3,results3) {								
								res.json({msg:"ok",nama_lengkap:results3[0].Nama_lengkap,id:results3[0].id}); //berhasil
							});
						}
						else res.json({msg:"not_ok2"}); //sudah milih (kena kondisi 2)
					});
					}
				}
		});
	}else res.json({msg:"not_ok1"});
};
