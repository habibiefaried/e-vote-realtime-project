<style type="text/css">
<!--
.style6 {font-size: 12px; font-weight: bold; }
-->
</style>
<form id="form1" name="form1" method="post" action="">
  <table width="626" border="0" align="center">
    <tr>
      <td><p align="right"><img src="image/kopsurat/c1.png" width="370" height="138" /></p>
        <p align="center">CATATAN PELAKSANAAN <br />
          PEMUNGUTAN SUARA DAN PENGHITUNGAN SUARA <br />
          PEMILIHAN UMUM KEPALA DAERAH DAN WAKIL KEPALA DAERAH <br />
          DI TEMPAT PEMUNGUTAN SUARA </p>
        <table width="576" border="0" bordercolor="#CCCCCC">
          <tr>
            <td valign="top">Pemilihan Umum </td>
            <td valign="top">:</td>
            <td>Gubernur dan Wakil Gubernur /Bupati dan Wakil <br />
              Bupati/Walikota dan Wakil Walikota *) </td>
          </tr>
          <tr>
            <td>Tempat Pemungutan Suara (TPS) </td>
            <td>:</td>
            <td><input name="textfield29" type="text" id="textfield29" size="50" /></td>
          </tr>
          <tr>
            <td>Desa/Kelurahan *)</td>
            <td>:</td>
            <td><input name="textfield30" type="text" id="textfield30" size="50" /></td>
          </tr>
          <tr>
            <td>Kecamatan</td>
            <td>:</td>
            <td><input name="textfield31" type="text" id="textfield31" size="50" /></td>
          </tr>
          <tr>
            <td>Kabupaten/Kota *)</td>
            <td>:</td>
            <td><input name="textfield32" type="text" id="textfield32" size="50" /></td>
          </tr>
          <tr>
            <td>Provinsi</td>
            <td>:</td>
            <td><input name="textfield33" type="text" id="textfield33" size="50" /></td>
          </tr>
        </table>
        <p>&nbsp;</p>
        <table width="629" border="1">
          <tr>
            <td colspan="5"><strong>A.  Data Pemilih </strong></td>
          </tr>
          <tr>
            <td width="24" rowspan="2"><div align="center"><strong>NO</strong></div></td>
            <td width="245" rowspan="2"><div align="center"><strong>URAIAN</strong></div></td>
            <td colspan="3"><div align="center"><strong>KETERANGAN </strong></div></td>
          </tr>
          <tr>
            <td width="110"><div align="center"><strong>LAKI-LAKI</strong></div></td>
            <td width="110"><div align="center"><strong>PEREMPUAN</strong></div></td>
            <td width="120"><div align="center"><strong>JUMLAH(3+4) </strong></div></td>
          </tr>
          <tr bgcolor="#CCCCCC">
            <td width="24"><div align="center" class="style6">1</div></td>
            <td width="245"><div align="center" class="style6">2</div></td>
            <td><div align="center" class="style6">3</div></td>
            <td><div align="center" class="style6">4</div></td>
            <td><div align="center" class="style6">5</div></td>
          </tr>
          <tr>
            <td valign="top"><div align="center">1</div></td>
            <td>Jumlah pemilih dalam Salinan Daftar Pemilih 
              Tetap (DPT) (A.2+A.3) </td>
            <td><div align="center">
                <input name="textfield34" type="text" id="textfield34" size="10" />
            </div></td>
            <td><div align="center">
                <input name="textfield36" type="text" id="textfield36" size="10" />
            </div></td>
            <td><div align="center">
                <input name="textfield38" type="text" id="textfield38" size="10" />
            </div></td>
          </tr>
          <tr>
            <td valign="top"><div align="center">2</div></td>
            <td>Jumlah pemilih dalam Salinan DPT yang <br />
              menggunakan hak pilih</td>
            <td><div align="center">
                <input name="textfield35" type="text" id="textfield35" size="10" />
            </div></td>
            <td><div align="center">
                <input name="textfield37" type="text" id="textfield37" size="10" />
            </div></td>
            <td><div align="center">
                <input name="textfield39" type="text" id="textfield39" size="10" />
            </div></td>
          </tr>
          <tr>
            <td valign="top"><div align="center">3</div></td>
            <td>Jumlah Pemilih dalam Salinan DPT yang <br />
              tidak menggunakan hak pilih </td>
            <td><div align="center">
                <input name="textfield40" type="text" id="textfield40" size="10" />
            </div></td>
            <td><div align="center">
                <input name="textfield41" type="text" id="textfield41" size="10" />
            </div></td>
            <td><div align="center">
                <input name="textfield42" type="text" id="textfield42" size="10" />
            </div></td>
          </tr>
          <tr>
            <td><div align="center">4</div></td>
            <td>Jumlah Pemilih dari TPS lain </td>
            <td><label>
                <div align="center">
                  <input name="textfield43" type="text" id="textfield43" size="10" />
                </div>
              </label></td>
            <td><div align="center">
                <input name="textfield44" type="text" id="textfield44" size="10" />
            </div></td>
            <td><div align="center">
                <input name="textfield45" type="text" id="textfield45" size="10" />
            </div></td>
          </tr>
        </table>
        <p>&nbsp;</p>
        <div align="right">
          <input name="textfield46" type="text" id="textfield46" size="10" />
,
<input name="textfield47" type="text" id="textfield47" size="15" />
20
<input name="textfield48" type="text" id="textfield48" size="2" />
        </div>
        <table width="237" border="1" align="right" bordercolor="#FFFFFF">
          <tr>
            <td width="227"><div align="center">KELOMPOK PENYELENGGARA <br />
              PEMUNGUTAN SUARA <br />
              KETUA,</div></td>
          </tr>
        </table>
        <p align="right">&nbsp;</p>
        <p align="right">&nbsp;</p>
        <p align="right"><br />
        </p>
        <p align="right">&nbsp;</p>
        <p align="right">(
          <input name="textfield5" type="text" id="textfield5" size="30" />
        )</p>
        <p align="right">&nbsp;</p>
        <p align="right">&nbsp;</p>
        <table width="300" border="0" align="center" bordercolor="#FFFFFF" bgcolor="#FFFFFF">
          
        </table>        
      <div align="center"></div></td>
    </tr>
  </table>
  <p>&nbsp;</p>
</form>