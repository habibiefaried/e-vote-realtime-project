<style type="text/css">
<!--
.style6 {font-size: 12px; font-weight: bold; }
-->
</style>
<form id="form1" name="form1" method="post" action="">
  <table width="626" border="0" align="center">
    <tr>
      <td><p align="right"><img src="image/kopsurat/c1lamp.png" width="370" height="138" /></p>
        <p align="center">SERTIFIKAT HASIL PENGHITUNGAN SUARA <br />
          UNTUK PASANGAN CALON KEPALA DAERAH DAN WAKIL <br />
          KEPALA DAERAH DI TEMPAT PEMUNGUTAN SUARA </p>
        <table width="592" border="0" bordercolor="#FFFFFF">
          <tr>
            <td width="207" valign="top">Pemilihan Umum</td>
            <td width="8" valign="top">:</td>
            <td width="355">Gubernur dan Wakil Gubernur /Bupati dan Wakil <br />
              Bupati/Walikota dan Wakil Walikota *) </td>
          </tr>
          <tr>
            <td>Tempat Pemungutan Suara (TPS) </td>
            <td>:</td>
            <td><input name="textfield49" type="text" id="textfield49" size="50" /></td>
          </tr>
          <tr>
            <td>Desa/Kelurahan *)</td>
            <td>:</td>
            <td><input name="textfield50" type="text" id="textfield50" size="50" /></td>
          </tr>
          <tr>
            <td>Kecamatan</td>
            <td>:</td>
            <td><input name="textfield51" type="text" id="textfield51" size="50" /></td>
          </tr>
          <tr>
            <td>Kabupaten/Kota *)</td>
            <td>:</td>
            <td><input name="textfield52" type="text" id="textfield52" size="50" /></td>
          </tr>
          <tr>
            <td>Provinsi</td>
            <td>:</td>
            <td><input name="textfield53" type="text" id="textfield53" size="50" /></td>
          </tr>
        </table>
        <p>&nbsp;</p>
        <table width="627" border="1" align="center">
          <tr bordercolor="#FFFFFF" bgcolor="#FFFFFF">
            <td colspan="3"><strong>A.  SUARA SAH (Diisi dari Huruf A Model C 2 – KWK.KPU) </strong></td>
          </tr>
          <tr>
            <td width="32"><div align="center"><strong>NO. </strong></div></td>
            <td width="211"><div align="center"><strong>NOMOR DAN NAMA <br />
              PASANGAN CALON <br />
              KEPALA DAERAH DAN <br />
              WAKIL KEPALA DAERAH </strong></div></td>
            <td width="362"><div align="center">
                <p><strong>PEROLEHAN SUARA SAH</strong><strong> PASANGAN CALON<br />
                  KEPALA DAERAH DAN WAKIL KEPALA
                  DAERAH </strong> </br>
                                  </br>
                                  </br>
                                  </br>
                </p>
            </div></td>
          </tr>
          <tr bgcolor="#CCCCCC">
            <td><div align="center" class="style6">1</div></td>
            <td><div align="center" class="style6">2</div></td>
            <td><div align="center" class="style6">3</div></td>
          </tr>
          <tr>
            <td valign="top">1.</td>
            <td><p align="center">
                <input name="textfield54" type="text" id="textfield54" size="35" />
                <br />
              dan </p>
                <p>
                  <input name="textfield55" type="text" id="textfield55" size="35" />
              </p></td>
            <td><table width="339" border="0" bordercolor="#FFFFFF">
                <tr>
                  <td width="115" valign="top">Tulis dengan angka </td>
                  <td width="8" valign="top">:</td>
                  <td width="194" valign="top"><input type="text" name="textfield56" id="textfield56" /></td>
                </tr>
                <tr>
                  <td valign="top">Tulis dengan huruf </td>
                  <td valign="top">:</td>
                  <td valign="top"><input type="text" name="textfield57" id="textfield57" /></td>
                </tr>
              </table>
            <p align="center">
                  <input name="textfield58" type="text" id="textfield58" size="30" />
              </p></td>
          </tr>
          <tr>
            <td valign="top">2.</td>
            <td><p align="center">
                <input name="textfield59" type="text" id="textfield59" size="35" />
                <br />
              dan </p>
                <p>
                  <input name="textfield59" type="text" id="textfield60" size="35" />
              </p></td>
            <td><table width="330" border="0" bordercolor="#FFFFFF">
                <tr>
                  <td width="115" valign="top">Tulis dengan angka </td>
                  <td width="8" valign="top">:</td>
                  <td width="185" valign="top"><input type="text" name="textfield60" id="textfield61" /></td>
                </tr>
                <tr>
                  <td height="26" valign="top">Tulis dengan huruf </td>
                  <td valign="top">:</td>
                  <td valign="top"><input type="text" name="textfield60" id="textfield62" /></td>
                </tr>
              </table>
            <p align="center">
                  <input name="textfield61" type="text" id="textfield63" size="30" />
              </p></td>
          </tr>
          <tr>
            <td valign="top">3.</td>
            <td><p align="center">
                <input name="textfield62" type="text" id="textfield64" size="35" />
                <br />
              dan </p>
                <p>
                  <input name="textfield62" type="text" id="textfield65" size="35" />
              </p></td>
            <td><table width="341" border="0" bordercolor="#FFFFFF">
                <tr>
                  <td width="115" valign="top">Tulis dengan angka </td>
                  <td width="8" valign="top">:</td>
                  <td width="196" valign="top"><input type="text" name="textfield64" id="textfield68" /></td>
                </tr>
                <tr>
                  <td height="26" valign="top">Tulis dengan huruf </td>
                  <td valign="top">:</td>
                  <td valign="top"><input type="text" name="textfield64" id="textfield69" /></td>
                </tr>
              </table>
            <p align="center">
                  <input name="textfield66" type="text" id="textfield72" size="30" />
              </p></td>
          </tr>
          <tr>
            <td valign="top">4.</td>
            <td><p align="center">
                <input name="textfield63" type="text" id="textfield66" size="35" />
                <br />
              dan </p>
                <p>
                  <input name="textfield63" type="text" id="textfield67" size="35" />
              </p></td>
            <td><table width="342" border="0" bordercolor="#FFFFFF">
                <tr>
                  <td width="332" height="26" valign="top"><table width="328" border="0" bordercolor="#FFFFFF">
                      <tr>
                        <td width="115" valign="top">Tulis dengan angka </td>
                        <td width="8" valign="top">:</td>
                        <td width="182" valign="top"><input type="text" name="textfield65" id="textfield70" /></td>
                      </tr>
                      <tr>
                        <td height="26" valign="top">Tulis dengan huruf </td>
                        <td valign="top">:</td>
                        <td valign="top"><input type="text" name="textfield65" id="textfield71" /></td>
                      </tr>
                  </table></td>
                </tr>
              </table>
          <p align="center">
                  <input name="textfield67" type="text" id="textfield73" size="30" />
              </p></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>Jumlah Perolehan Suara Sah untuk <br />
              Seluruh Pasangan Calon</td>
            <td><table width="320" border="0" bordercolor="#FFFFFF">
                <tr>
                  <td width="115" valign="top">Tulis dengan angka </td>
                  <td width="8" valign="top">:</td>
                  <td width="175" valign="top"><input type="text" name="textfield68" id="textfield74" /></td>
                </tr>
                <tr>
                  <td height="26" valign="top">Tulis dengan huruf </td>
                  <td valign="top">:</td>
                  <td valign="top"><input type="text" name="textfield68" id="textfield75" /></td>
                </tr>
            </table></td>
          </tr>
        </table>
        <p>*) Coret yang tidak perlu </p>
        <table width="637" border="1">
          <tr>
            <td colspan="7"><div align="center"><strong>TANDA TANGAN KPPS </strong></div></td>
          </tr>
          <tr>
            <td width="83">1.</td>
            <td width="83">2.</td>
            <td width="83">3.</td>
            <td width="83">4.</td>
            <td width="83">5.</td>
            <td width="83">6.</td>
            <td width="93">7.</td>
          </tr>
          <tr>
            <td colspan="7"><div align="center"><strong>TANDA TANGAN SAKSI PASANGAN CALON KEPALA DAERAH DAN WAKIL KEPALA DAERAH </strong></div></td>
          </tr>
          <tr>
            <td>1.</td>
            <td>2.</td>
            <td>3.</td>
            <td>4.</td>
            <td>5.</td>
            <td>6.</td>
            <td>7.</td>
          </tr>
        </table>
        <br>&nbsp;</br>
        <table width="300" border="0" align="center" bordercolor="#FFFFFF" bgcolor="#FFFFFF">
         
        </table>
      </td>
    </tr>
  </table>
</form>