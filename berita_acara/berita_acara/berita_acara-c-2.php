<form id="form1" name="form1" method="post" action="">
  <table width="626" border="0" align="center">
    <tr>
      <td><table width="564" border="0" bordercolor="#FFFFFF">
        <tr>
          <td width="14">II.</td>
          <td width="534">Penghitungan Suara (mulai pukul
            <input name="textfield9" type="text" id="textfield9" size="5" />
            s/d
            <input name="textfield10" type="text" id="textfield10" size="5" />
            ) </td>
        </tr>
      </table>
        <table width="563" border="0" bordercolor="#FFFFFF">
          <tr>
            <td width="33" valign="top"><div align="right">A.</div></td>
            <td width="514">Persiapan sebelum pelaksanaan penghitungan suara KPPS melakukan kegiatan <br />
            sebagai berikut : </td>
          </tr>
        </table>
        <table width="564" border="0" bordercolor="#FFFFFF">
          <tr>
            <td width="52" valign="top"><div align="right">1.</div></td>
            <td width="496">Mengumumkan dan mencatat jumlah pemilih yang memberikan suara dan yang <br />
              tidak memberikan suara berdasarkan salinan Daftar Pemilih Tetap untuk TPS <br />
            serta jumlah pemilih dari TPS lain; </td>
          </tr>
          <tr>
            <td valign="top"><div align="right">2. </div></td>
            <td>Menampilkan catatan hasil perolehan suara untuk tiap Pasangan Calon Kepala <br />
              Daerah dan Wakil Kepala Daerah di Tempat Pemungutan Suara (Model C2–<br />
              KWK.KPU) pada display yang tersedia di masing-masing TPS.</td>
          </tr>
        </table>
        <table width="559" border="0" bordercolor="#FFFFFF">
          <tr>
            <td width="32" valign="top"><div align="right">B.</div></td>
            <td width="511">Pelaksanaan pencocokan perhitungan suara. <br />
            KPPS melakukan kegiatan sebagai berikut : </td>
          </tr>
        </table>
        <table width="560" border="0" bordercolor="#FFFFFF">
          <tr>
            <td width="51" valign="top"><div align="right">1.</div></td>
            <td width="493">Membuka kotak suara, menghitung, meneliti dan menyamakan hasil perhitungan perolehan suara hasil perhitungan mesin e-voting.</td>
          </tr>
        </table>
        <table width="565" border="0" bordercolor="#FFFFFF">
          <tr>
            <td width="19">III.</td>
            <td width="12">A.</td>
            <td width="512"> Lampiran Berita Acara : </td>
          </tr>
        </table>
        <table width="565" border="0" bordercolor="#FFFFFF">
          <tr>
            <td width="50" valign="top"><div align="right">1.</div></td>
            <td width="499">Catatan Pelaksanaan Pemungutan Suara dan Penghitungan Suara Untuk Pemilihan Umum Kepala Daerah dan  Wakil Kepala Daerah di Tempat
            Pemungutan Suara (Model C 1 - KWK.KPU); </td>
          </tr>
          <tr>
            <td valign="top"><div align="right">2.</div></td>
            <td>Sertifikat Hasil Penghitungan Suara Pemilihan Umum Kepala Daerah dan Wakil <br />
              Kepala Daerah di Tempat Pemungutan Suara (Lampiran Model C 1 - KWK.KPU); </td>
          </tr>
          <tr>
            <td valign="top"><div align="right">3.</div></td>
            <td>Hasil Perolehan Suara Untuk Tiap Pasangan Calon Kepala Daerah dan Wakil <br />
              Kepala Daerah di Tempat Pemungutan Suara (Model C 2 - KWK.KPU) ukuran <br />
              besar; </td>
          </tr>
          <tr>
            <td valign="top"><div align="right">4.</div></td>
            <td>Pernyataan Keberatan Saksi dan Kejadian Khusus Yang Berhubungan Dengan <br />
              Hasil Pemungutan Suara dan Penghitungan Suara Pemilihan Umum Kepala
              Daerah dan Wakil Kepala Daerah di Tempat Pemungutan Suara (Model C 3 - <br />
              KWK.KPU); </td>
          </tr>
          <tr>
            <td valign="top"><div align="right">5.</div></td>
            <td><div align="justify">Catatan Pembukaan Kotak Suara, Pengeluaran Isi, Identifikasi Jenis Dokumen, <br />
              dan Penghitungan Jumlah Setiap Jenis Dokumen Untuk Pelaksanaan Pemungutan <br />
              Suara Pemilihan Umum Kepala Daerah dan Wakil Kepala Daerah di Tempat <br />
              Pemungutan Suara (Model C 4 - KWK.KPU); </div></td>
          </tr>
          <tr>
            <td valign="top"><div align="right">6.</div></td>
            <td>Penggunaan Surat Suara Cadangan Dalam Pemungutan Suara di Tempat <br />
              Pemungutan Suara (Model C 5 – KWK.KPU);</td>
          </tr>
        </table>
        <table width="589" border="0" bordercolor="#FFFFFF">
          <tr>
            <td width="43" valign="top"><div align="right">B.</div></td>
            <td width="530">Lampiran Berita Acara sebagaimana dimaksud pada huruf A merupakan bagian yang <br />
            tidak terpisahkan dari Berita Acara ini. </td>
          </tr>
        </table>
        <br />
        <table width="581" border="0" bordercolor="#FFFFFF">
          <tr>
            <td width="22" valign="top">IV.</td>
            <td width="543"><p>Kelengkapan administrasi lain yang tidak  termasuk dalam Lampiran Berita Acara dan <br />
              dikirimkan kepada PPS : </p>
              <p>&nbsp;</p></td>
          </tr>
        </table>
      
        <table width="300" border="0" align="center" bordercolor="#FFFFFF" bgcolor="#FFFFFF">
          
        </table>
      </tr>
  </table>
</form>