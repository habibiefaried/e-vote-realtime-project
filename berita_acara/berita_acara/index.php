<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Berita Acara Pemilihan</title>
<script src="../SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	background-image: url(images/background.gif);
}
#back_menu a{
	text-decoration: none;
	font-size: 16px;
	color: #CC3;
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
}

#back_menu a:hover{
	text-decoration: none;
	font-size: 16px;
	color: #F00;
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
}

-->
</style>
</head>

<body>
<div id="back_menu">
<a href="menu_admsite.php" >Administrator Area</a>
</div>
<br />
<div id="TabBeritaAcara" class="TabbedPanels">
  <ul class="TabbedPanelsTabGroup">
    <li class="TabbedPanelsTab" tabindex="0">C</li>
    <li class="TabbedPanelsTab" tabindex="0">C1</li>
    <li class="TabbedPanelsTab" tabindex="0">C1 Lampiran</li>
    <li class="TabbedPanelsTab" tabindex="0">C2</li>
    <li class="TabbedPanelsTab" tabindex="0">C3</li>
    <li class="TabbedPanelsTab" tabindex="0">C4</li>
    <li class="TabbedPanelsTab" tabindex="0">C6</li>
    <li class="TabbedPanelsTab" tabindex="0">C7</li>
    <li class="TabbedPanelsTab" tabindex="0">C8</li>
    <li class="TabbedPanelsTab" tabindex="0">C9</li>
    <li class="TabbedPanelsTab" tabindex="0">C10</li>
  </ul>
  <div class="TabbedPanelsContentGroup">
    <div class="TabbedPanelsContent">
	<?php 
	  include "berita_acara-c-1.php"; 
	  include "berita_acara-c-2.php"; 
	  include "berita_acara-c-3.php";
	  echo "<br /><br /><div align='center'><INPUT name='print_c' value='Print C' TYPE='button' onClick='window.print()'></div>";
	?>
    </div>
    <div class="TabbedPanelsContent">
	<?php 
	  include "berita_acara-c1.php"; 
	  echo "<br /><br /><div align='center'><INPUT name='print_c1' value='Print C1' TYPE='button' onClick='window.print()'></div>";
	?>
    </div>
    <div class="TabbedPanelsContent">
	<?php 
	  include "berita_acara-c1lamp.php"; 
	  include "berita_acara-c1lamp2.php";
	  echo "<br /><br /><div align='center'><INPUT name='print_c1_lampiran' value='Print C1 Lampiran' TYPE='button' onClick='window.print()'></div>";
	?>
    </div>
    <div class="TabbedPanelsContent">
	<?php 
	  include "berita_acara-c2-1.php"; 
	  include "berita_acara-c2-2.php";
	  echo "<br /><br /><div align='center'><INPUT name='print_c2' value='Print C2' TYPE='button' onClick='window.print()'></div>";
	?>
    </div>
    <div class="TabbedPanelsContent">
	<?php 
	  include "berita_acara-c3.php"; 
	  echo "<br /><br /><div align='center'><INPUT name='print_c3' value='Print C3' TYPE='button' onClick='window.print()'></div>";
	?>
    </div>
    <div class="TabbedPanelsContent">
	<?php 
	  include "berita_acara-c4.php"; 
	  echo "<br /><br /><div align='center'><INPUT name='print_c4' value='Print C4' TYPE='button' onClick='window.print()'></div>";
	?>
    </div>
    <div class="TabbedPanelsContent">
	<?php 
	  include "berita_acara-c6.php"; 
	  echo "<br /><br /><div align='center'><INPUT name='print_c6' value='Print C6' TYPE='button' onClick='window.print()'></div>";
	?>
    </div>
    <div class="TabbedPanelsContent">
	<?php 
	  include "berita_acara-c7.php";
	  echo "<br /><br /><div align='center'><INPUT name='print_c7' value='Print C7' TYPE='button' onClick='window.print()'></div>";
	?>
    </div>
    <div class="TabbedPanelsContent">
	<?php 
	  include "berita_acara-c8.php";
	  echo "<br /><br /><div align='center'><INPUT name='print_c8' value='Print C8' TYPE='button' onClick='window.print()'></div>";
	?>
    </div>
    <div class="TabbedPanelsContent">
	<?php 
	  include "berita_acara-c9.php";
	  echo "<br /><br /><div align='center'><INPUT name='print_c9' value='Print C9' TYPE='button' onClick='window.print()'></div>";
	?>
    </div>
    <div class="TabbedPanelsContent">
	<?php 
	  include "berita_acara-c10.php";
	  echo "<br /><br /><div align='center'><INPUT name='print_c10' value='Print C10' TYPE='button' onClick='window.print()'></div>";
	?>
    </div>
  </div>
</div>
<script type="text/javascript">
<!--
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabBeritaAcara");
//-->
</script>
</body>
</html>