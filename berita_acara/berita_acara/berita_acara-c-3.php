<form id="form1" name="form1" method="post" action="">
  <table width="626" border="0" align="center">
    <tr>
      <td> 
        <table width="590" border="0" bordercolor="#FFFFFF">
          <tr>
            <td width="50" valign="top"><div align="right">1.</div></td>
            <td width="524">Surat Pemberitahuan Waku dan Tempat Pemungutan Suara (Model C 6 - KWK.KPU) 
            yang diterima KPPS dari pemilih; </td>
          </tr>
          <tr>
            <td valign="top"><div align="right">2.</div></td>
            <td>Surat Pernyataan Pendamping Pemilih (Model C 7 - KWK.KPU) yang diterima KPPS 
              dari pemilih; </td>
          </tr>
          <tr>
            <td valign="top"><div align="right">3.</div></td>
            <td>Daftar Nama Pemilih Yang Memberikan Suara dari TPS lain (Model C 8 - KWK.KPU); </td>
          </tr>
          <tr>
            <td><div align="right">4.</div></td>
            <td>Surat Pengantar (Model C 9 - KWK.KPU); dan </td>
          </tr>
          <tr>
            <td><div align="right">5.</div></td>
            <td>Tanda Terima ( Model C 10 – KWK.KPU). </td>
          </tr>
        </table>
        <br />
        <table width="573" border="0" bordercolor="#FFFFFF">
          <tr>
            <td width="21">V.</td>
            <td width="536">Penyampaian Berita Acara dan Lampiran Model C1 – KWK.KPU : </td>
          </tr>
        </table>
        <table width="590" border="0" bordercolor="#FFFFFF">
          <tr>
            <td width="35" valign="top"><div align="right">A.</div></td>
            <td width="539">Berita acara pemungutan suara dan penghitungan suara di TPS beserta lampirannya <br />
              dibuat
              <input name="textfield11" type="text" id="textfield11" size="5" />
              (
              <input name="textfield12" type="text" id="textfield12" size="20" />
            ) rangkap : </td>
          </tr>
        </table>
        <table width="591" border="0" bordercolor="#FFFFFF">
          <tr>
            <td width="48"><div align="right">1.</div></td>
            <td width="527">1 (satu) rangkap untuk Panitia Pemungutan Suara; </td>
          </tr>
          <tr>
            <td valign="top"><div align="right">2.</div></td>
            <td>1 (satu) rangkap untuk Panitia Pemilihan Kecamatan (PPK) melalui Panitia Pemungutan Suara (PPS); dan </td>
          </tr>
          <tr>
            <td valign="top"><div align="right">3.</div></td>
            <td><input name="textfield13" type="text" id="textfield13" size="5" />
              (
              <input type="text" name="textfield14" id="textfield14" />
              ) rangkap untuk masing-masing saksi pasangan calon Kepala Daerah dan 
              calon Wakil Kepala Daerah yang hadir. </td>
          </tr>
          <tr>
            <td valign="top"><div align="right">4.</div></td>
            <td>1 (satu) rangkap untuk Pengawas Pemilu Lapangan. </td>
          </tr>
        </table>
        <table width="601" border="0" bordercolor="#FFFFFF">
          <tr>
            <td width="34" valign="top"><div align="right">B.</div></td>
            <td width="551"><div align="justify">Sertifikat Hasil Penghitungan Suara Pemilihan Umum Kepala Daerah dan Wakil <br />
              Kepala Daerah di Tempat Pemungutan Suara (Lampiran Model C 1 - KWK.KPU) <br />
            selain hal tersebut pada huruf A 1 (satu) rangkap untuk pengumuman di PPS. </div></td>
          </tr>
        </table>
        <div align="center">
          <p><br />
              <br />
              <strong>KELOMPOK PENYELENGGARA PEMUNGUTAN SUARA</strong><br />
          </p>
          <table width="619" border="0" bordercolor="#FFFFFF">
            <tr>
              <td width="44"><strong>NO</strong></td>
              <td width="212"><div align="center"><strong>Jabatan</strong></div></td>
              <td width="243"><div align="center"><strong>NAMA</strong></div></td>
              <td width="190"><div align="center"><strong>TANDA TANGAN</strong></div></td>
            </tr>
            <tr>
              <td>1.</td>
              <td>Ketua</td>
              <td><div align="center">
                  <input name="textfield15" type="text" id="textfield15" size="40" />
              </div></td>
              <td><div align="center">(.............................................)</div></td>
            </tr>
            <tr>
              <td>2.</td>
              <td>Angoota</td>
              <td><div align="center">
                  <input name="textfield16" type="text" id="textfield16" size="40" />
              </div></td>
              <td><div align="center">(.............................................)</div></td>
            </tr>
            <tr>
              <td>3.</td>
              <td>Anggota</td>
              <td><div align="center">
                  <input name="textfield17" type="text" id="textfield17" size="40" />
              </div></td>
              <td><div align="center">(.............................................)</div></td>
            </tr>
            <tr>
              <td>4.</td>
              <td>Anggota</td>
              <td><div align="center">
                  <input name="textfield18" type="text" id="textfield18" size="40" />
              </div></td>
              <td><div align="center">(.............................................)</div></td>
            </tr>
          </table>
          <br />
          <br />
          <strong>Saksi-saksi dari pasangan calon Kepala Daerah dan calon Wakil Kepala Daerah</strong> <br />
          <br />
          <table width="610" border="0" bordercolor="#FFFFFF">
            <tr>
              <td width="38"><div align="center"><strong>NO</strong></div></td>
              <td width="172"><div align="center"><strong>Nama</strong></div></td>
              <td width="191"><div align="center"><strong>Saksi dari nomor urut <br />
                pasangan calon Kepala <br />
                Daerah dan calon Wakil <br />
              Kepala Daerah </strong></div></td>
              <td width="181"><div align="center"><strong>Tanda Tangan </strong></div></td>
            </tr>
            <tr>
              <td>1.</td>
              <td><div align="center">
                  <input type="text" name="textfield19" id="textfield19" />
              </div></td>
              <td><div align="center">
                  <input type="text" name="textfield24" id="textfield24" />
              </div></td>
              <td><div align="center">(.............................................)</div></td>
            </tr>
            <tr>
              <td>2.</td>
              <td><div align="center">
                  <input type="text" name="textfield20" id="textfield20" />
              </div></td>
              <td><div align="center">
                  <input type="text" name="textfield25" id="textfield25" />
              </div></td>
              <td><div align="center">(.............................................)</div></td>
            </tr>
            <tr>
              <td>3.</td>
              <td><div align="center">
                  <input type="text" name="textfield21" id="textfield21" />
              </div></td>
              <td><label>
                <div align="center">
                  <input type="text" name="textfield26" id="textfield26" />
                  </div>
                </label></td>
              <td><div align="center">(.............................................)</div></td>
            </tr>
            <tr>
              <td>4.</td>
              <td><div align="center">
                  <input type="text" name="textfield22" id="textfield22" />
              </div></td>
              <td><div align="center">
                  <input type="text" name="textfield27" id="textfield27" />
              </div></td>
              <td><div align="center">(.............................................)</div></td>
            </tr>
            <tr>
              <td>5.</td>
              <td><div align="center">
                  <input type="text" name="textfield23" id="textfield23" />
              </div></td>
              <td><div align="center">
                  <input type="text" name="textfield28" id="textfield28" />
              </div></td>
              <td><div align="center">(.............................................)</div></td>
            </tr>
          </table>
          <p>&nbsp;</p>
          <table width="300" border="0" bordercolor="#FFFFFF" bgcolor="#FFFFFF">
            
          </table>
        </div>
      </td>
    </tr>
  </table>
</form>