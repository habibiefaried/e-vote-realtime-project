<style type="text/css">
<!--
.style7 {font-size: 12px}
-->
</style>
<form id="form1" name="form1" method="post" action="">
  <table width="626" border="0" align="center">
    <tr>
      <td>&nbsp;</br>
        <table width="640" border="1">
          <tr bordercolor="#FFFFFF">
            <td colspan="3"><strong>B.  SUARA TIDAK SAH (Diisi dari Huruf B Model C 2 – KWK.KPU) </strong></td>
          </tr>
          <tr>
            <td width="30"><div align="center"><strong>No. </strong></div></td>
            <td width="247"><div align="center"><strong>URAIAN </strong></div></td>
            <td width="341"><div align="center"><strong>JUMLAH SUARA TIDAK SAH</strong></div></td>
          </tr>
          <tr bgcolor="#CCCCCC">
            <td><div align="center"><strong><span class="style7">1</span></strong></div></td>
            <td><div align="center"><strong><span class="style7">2</span></strong></div></td>
            <td><div align="center"><strong><span class="style7">3</span></strong></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><div align="center">SUARA TIDAK SAH </div></td>
            <td><table width="320" border="0" bordercolor="#FFFFFF">
                <tr>
                  <td width="115" valign="top">Tulis dengan angka </td>
                  <td width="8" valign="top">:</td>
                  <td width="175" valign="top"><input type="text" name="textfield69" id="textfield76" /></td>
                </tr>
                <tr>
                  <td height="26" valign="top">Tulis dengan huruf </td>
                  <td valign="top">:</td>
                  <td valign="top"><input type="text" name="textfield69" id="textfield77" /></td>
                </tr>
              </table>
            <p align="center">
                  <input name="textfield70" type="text" id="textfield78" size="30" />
              </p></td>
          </tr>
        </table>
        <p>&nbsp;</p>
        <table width="640" border="1">
          <tr bordercolor="#FFFFFF">
            <td colspan="3"><strong>C.  JUMLAH SUARA SAH DAN TIDAK SAH</strong></td>
          </tr>
          <tr>
            <td width="30"><div align="center"><strong>No. </strong></div></td>
            <td width="247"><div align="center"><strong>URAIAN </strong></div></td>
            <td width="341"><div align="center"><strong>JUMLAH SUARA  SAH DAN TIDAK SAH </strong></div></td>
          </tr>
          <tr bgcolor="#CCCCCC">
            <td><div align="center"><strong><span class="style7">1</span></strong></div></td>
            <td><div align="center"><strong><span class="style7">2</span></strong></div></td>
            <td><div align="center"><strong><span class="style7">3</span></strong></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><div align="center">JUMLAH SUARA SAH dan TIDAK<br />
              SAH </div></td>
            <td><table width="320" border="0" bordercolor="#FFFFFF">
                <tr>
                  <td width="115" valign="top">Tulis dengan angka </td>
                  <td width="8" valign="top">:</td>
                  <td width="175" valign="top"><input type="text" name="textfield71" id="textfield79" /></td>
                </tr>
                <tr>
                  <td height="26" valign="top">Tulis dengan huruf </td>
                  <td valign="top">:</td>
                  <td valign="top"><input type="text" name="textfield71" id="textfield80" /></td>
                </tr>
            </table></td>
          </tr>
        </table>
        <p align="center"><br />
        </p>
        <table width="643" border="0" bordercolor="#FFFFFF">
          <tr>
            <td width="64" valign="top">Catatan</td>
            <td width="11" valign="top">: </td>
            <td width="546">Apabila terdapat kesalahan penulisan angka dan huruf dalam kolom 3, dicoret
              angka dan huruf yang salah, kemudian angka dan huruf yang benar diperbaiki 
            dan harus diparaf oleh Ketua KPPS. </td>
          </tr>
        </table>
      <p align="center"> <br />
            <strong>KELOMPOK PENYELENGGARA PEMUNGUTAN SUARA </strong><br />
        </p>
        <table width="619" border="0" bordercolor="#FFFFFF">
          <tr>
            <td width="44"><strong>NO</strong></td>
            <td width="212"><div align="center"><strong>Jabatan</strong></div></td>
            <td width="243"><div align="center"><strong>NAMA</strong></div></td>
            <td width="190"><div align="center"><strong>TANDA TANGAN</strong></div></td>
          </tr>
          <tr>
            <td>1.</td>
            <td>Ketua</td>
            <td><div align="center">
                <input name="textfield72" type="text" id="textfield81" size="40" />
            </div></td>
            <td><div align="center">(.............................................)</div></td>
          </tr>
          <tr>
            <td>2.</td>
            <td>Angoota</td>
            <td><div align="center">
                <input name="textfield72" type="text" id="textfield82" size="40" />
            </div></td>
            <td><div align="center">(.............................................)</div></td>
          </tr>
          <tr>
            <td>3.</td>
            <td>Anggota</td>
            <td><div align="center">
                <input name="textfield72" type="text" id="textfield83" size="40" />
            </div></td>
            <td><div align="center">(.............................................)</div></td>
          </tr>
          <tr>
            <td>4.</td>
            <td>Anggota</td>
            <td><div align="center">
                <input name="textfield72" type="text" id="textfield84" size="40" />
            </div></td>
            <td><div align="center">(.............................................)</div></td>
          </tr>
        </table>
        <p align="center"><strong>Saksi-saksi dari pasangan calon Kepala Daerah dan calon Wakil Kepala Daerah </strong><br />
        </p>
        <table width="610" border="0" bordercolor="#FFFFFF">
          <tr>
            <td width="38"><div align="center"><strong>NO</strong></div></td>
            <td width="172"><div align="center"><strong>Nama</strong></div></td>
            <td width="191"><div align="center"><strong>Saksi dari nomor urut <br />
              pasangan calon Kepala <br />
              Daerah dan calon Wakil <br />
            Kepala Daerah </strong></div></td>
            <td width="181"><div align="center"><strong>Tanda Tangan </strong></div></td>
          </tr>
          <tr>
            <td>1.</td>
            <td><div align="center">
                <input type="text" name="textfield73" id="textfield85" />
            </div></td>
            <td><div align="center">
                <input type="text" name="textfield73" id="textfield86" />
            </div></td>
            <td><div align="center">(.............................................)</div></td>
          </tr>
          <tr>
            <td>2.</td>
            <td><div align="center">
                <input type="text" name="textfield73" id="textfield87" />
            </div></td>
            <td><div align="center">
                <input type="text" name="textfield73" id="textfield88" />
            </div></td>
            <td><div align="center">(.............................................)</div></td>
          </tr>
          <tr>
            <td>3.</td>
            <td><div align="center">
                <input type="text" name="textfield73" id="textfield89" />
            </div></td>
            <td><label>
              <div align="center">
                <input type="text" name="textfield73" id="textfield90" />
                </div>
              </label></td>
            <td><div align="center">(.............................................)</div></td>
          </tr>
          <tr>
            <td>4.</td>
            <td><div align="center">
                <input type="text" name="textfield73" id="textfield91" />
            </div></td>
            <td><div align="center">
                <input type="text" name="textfield73" id="textfield92" />
            </div></td>
            <td><div align="center">(.............................................)</div></td>
          </tr>
          <tr>
            <td>5.</td>
            <td><div align="center">
                <input type="text" name="textfield73" id="textfield93" />
            </div></td>
            <td><div align="center">
                <input type="text" name="textfield73" id="textfield94" />
            </div></td>
            <td><div align="center">(.............................................)</div></td>
          </tr>
        </table>
        <br>&nbsp;</br>
        <br>&nbsp;</br>
        <table width="300" border="0" align="center" bordercolor="#FFFFFF" bgcolor="#FFFFFF">
          
        </table>
      <p align="center"></p></td>
    </tr>
  </table>
</form>