<style type="text/css">
<!--
.style2 {color: #000000; }
-->
</style>
<form id="form1" name="form1" method="post" action="">
  <table width="626" border="0" align="center">
    <tr>
      <td width="657"><p align="right"><img src="image/kopsurat/c.png" width="370" height="138" /></p>
        <p align="center">BERITA ACARA <br />
          PEMUNGUTAN SUARA DAN PENGHITUNGAN SUARA <br />
          PEMILIHAN UMUM KEPALA DAERAH DAN WAKIL KEPALA DAERAH <br />
          DI TEMPAT PEMUNGUTAN SUARA </p>
        <p align="justify"> Pada hari ini  tanggal
          <input name="textfield6" type="text" id="textfield6" size="2" maxlength="2" />
          bulan
  <input name="textfield7" type="text" id="textfield7" size="10" />
          tahun dua ribu
  <input name="textfield8" type="text" id="textfield8" size="4" />
          , Kelompok Penyelenggara Pemungutan Suara  (KPPS) melaksanakan rapat pemungutan suara 
          Pemilihan Umum Kepala Daerah dan Wakil Kepala Daerah yang dihadiri oleh saksi pasangan 
          calon, panitia pengawas lapangan, pemantau dan warga masyarakat bertempat di: <br />
        </p>
        <table width="626" border="0" bordercolor="#FFFFFF">
          <tr>
            <td width="229"><div align="right" class="style2">
                <div align="left">Tempat Pemungutan Suara (TPS) </div>
            </div></td>
            <td width="10"><div align="center" class="style2">:</div></td>
            <td width="365"><input name="textfield" type="text" id="textfield" size="50" /></td>
          </tr>
          <tr>
            <td><div align="right" class="style2">
                <div align="left">PPS/Desa/Kelurahan </div>
            </div></td>
            <td><div align="center" class="style2">:</div></td>
            <td><input name="textfield2" type="text" id="textfield2" size="50" /></td>
          </tr>
          <tr>
            <td><div align="right" class="style2">
                <div align="left">Kecamatan </div>
            </div></td>
            <td><div align="center" class="style2">:</div></td>
            <td><input name="textfield3" type="text" id="textfield3" size="50" /></td>
          </tr>
          <tr>
            <td><div align="right" class="style2">
                <div align="left">Kabupaten/Kota</div>
            </div></td>
            <td><div align="center" class="style2">:</div></td>
            <td><input name="textfield4" type="text" id="textfield4" size="50" /></td>
          </tr>
          <tr>
            <td><div align="right" class="style2">
                <div align="left">Provinsi</div>
            </div></td>
            <td><div align="center" class="style2">:</div></td>
            <td><input name="textfield5" type="text" id="textfield5" size="50" /></td>
          </tr>
        </table>
        <p align="justify">Telah melaksanakan kegiatan sebagai berikut : </p>
        <table width="463" border="0" bordercolor="#FFFFFF">
          <tr>
            <td width="15">I.</td>
            <td width="432">Pemungutan Suara </td>
          </tr>
        </table>
        <table width="400" border="0" bordercolor="#FFFFFF">
          <tr>
            <td width="37"><div align="right">A.</div></td>
            <td width="347">Persiapan (Pukul 06.00 s/d 07.00) </td>
          </tr>
        </table>
        <table width="566" border="0" bordercolor="#FFFFFF">
          <tr>
            <td width="54" valign="top"><div align="right">1.</div></td>
            <td width="496" valign="top"> Pemeriksaan TPS, pemasangan Daftar Calon Kepala Daerah dan Wakil Kepala Daerah, meletakkan bilik suara dan kotak suara sesuai dengan tempat yang telah
              ditentukan; </td>
          </tr>
          <tr>
            <td valign="top"><div align="right">2.</div></td>
            <td valign="top">Pemanggilan pemilih untuk memasuki  TPS, sebanyak tempat duduk yang <br />
              disediakan; dan </td>
          </tr>
          <tr>
            <td valign="top"><div align="right">3.</div></td>
            <td valign="top"> Penerimaan saksi sesuai dengan surat mandat dari Tim Pelaksana Kampanye.</td>
          </tr>
        </table>
        <table width="575" border="0" bordercolor="#FFFFFF">
          <tr>
            <td width="36"><div align="right">B.</div></td>
            <td width="523">Pelaksanaan pemungutan suara (Pukul 07.00 s/d 13.00) : </td>
          </tr>
        </table>
        <table width="571" border="0" bordercolor="#FFFFFF">
          <tr>
            <td width="53"><div align="right">1.</div></td>
            <td width="502">Ketua KPPS membuka Rapat Pemungutan Suara pada pukul 07.00; </td>
          </tr>
          <tr>
            <td><div align="right">2. </div></td>
            <td>Pengucapan sumpah/janji Anggota KPPS, dipandu oleh Ketua KPPS; </td>
          </tr>
          <tr>
            <td valign="top"><div align="right">3.</div></td>
            <td>Pembukaan kotak suara, pengeluaran seluruh isi kotak suara, pengidentifikasian <br />
              jenis dokumen dan peralatan serta penghitungan jumlah setiap jenis dokumen <br />
              dan peralatan; </td>
          </tr>
          <tr>
            <td valign="top"><div align="right">4.</div></td>
            <td>Ketua KPPS mengumumkan jumlah pemilih yang tercantum dalam salinan daftar 
              pemilih tetap untuk TPS; dan </td>
          </tr>
          <tr bordercolor="#FFFFFF">
            <td valign="top"><div align="right">5.</div></td>
            <td>Ketua KPPS memberikan penjelasan mengenai tata cara pemungutan suara/ <br />
              pemberian suara kepada pemilih yang hadir. </td>
          </tr>
        </table>
        <table width="554" border="0" bordercolor="#FFFFFF">
          <tr bordercolor="#FFFFFF">
            <td width="34"><div align="right">C. </div></td>
            <td width="504">Pemberian suara oleh pemilih berdasarkan prinsip urutan kehadiran. </td>
          </tr>
          <tr bordercolor="#FFFFFF">
            <td valign="top"><div align="right">D. </div></td>
            <td>Pada pukul 13.00 Ketua KPPS mengumumkan rapat pemungutan suara telah selesai 
            dan dilanjutkan dengan rapat penghitungan suara. <br /></td>
          </tr>
        </table>
      
        <br>&nbsp;</br>
        <table width="300" border="0" align="center" bordercolor="#FFFFFF" bgcolor="#FFFFFF">
          
        </table>
      </p></td>
    </tr>
  </table>
</form>