<form id="form1" name="form1" method="post" action="">
  <table width="626" border="0" align="center">
    <tr>
      <td><p align="right"><img src="image/kopsurat/c9.png" width="370" height="138" /></p>
        <table width="291" border="0" align="left">
          <tr>
            <td width="55" valign="top">Perihal</td>
            <td width="8" valign="top">:</td>
            <td width="206">Penyampaian Berita Acara<br />
              pemungutan suara dan<br />
              penghitungan suara di TPS<br />
              <input name="textfield47" type="text" id="textfield47" size="20" /></td>
          </tr>
        </table>
        <table width="286" border="0" align="right">
          <tr>
            <td width="23">&nbsp;</td>
            <td colspan="3">Kepada</td>
          </tr>
          <tr>
            <td colspan="4" valign="top">Yth. Ketua PPK 
            <input name="textfield" type="text" id="textfield" size="15" /></td>
          </tr>
          <tr>
            <td colspan="2" rowspan="4" valign="top">&nbsp;</td>
            <td colspan="2" valign="top">melalui PPS 
            <input name="textfield2" type="text" id="textfield2" size="15" /></td>
          </tr>
          <tr>
            <td colspan="2" valign="top"><input name="textfield3" type="text" id="textfield3" size="28" /></td>
          </tr>
          <tr>
            <td colspan="2" valign="top">di-</td>
          </tr>
          <tr>
            <td width="23">&nbsp;</td>
            <td width="224">Tempat</td>
          </tr>
        </table>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <table width="590" border="0" align="right">
          <tr>
            <td colspan="6">Bersama ini disampaikan Berita Acara beserta lampiran dalam pelaksanaan pemungutan<br />
              suara dan penghitungan suara di :</td>
          </tr>
          <tr>
            <td colspan="4">Tempat Pemungutan Suara (TPS)</td>
            <td width="11">:</td>
            <td width="338"><input name="textfield92" type="text" id="textfield138" size="50" /></td>
          </tr>
          <tr>
            <td colspan="4">Desa/Kelurahan</td>
            <td>:</td>
            <td><input name="textfield93" type="text" id="textfield139" size="50" /></td>
          </tr>
          <tr>
            <td colspan="4">Kecamatan</td>
            <td>:</td>
            <td><input name="textfield94" type="text" id="textfield140" size="50" /></td>
          </tr>
          <tr>
            <td colspan="4">Kabupaten/Kota</td>
            <td>:</td>
            <td><input name="textfield95" type="text" id="textfield141" size="50" /></td>
          </tr>
          <tr>
            <td colspan="4">Provinsi</td>
            <td>:</td>
            <td><input name="textfield96" type="text" id="textfield142" size="50" /></td>
          </tr>
          <tr>
            <td colspan="6">Jenis kelengkapan administrasi dan formulir pemungutan suara dan penghitungan suara di tempat<br />
              pemungutan suara, terdiri dari :</td>
          </tr>
          <tr>
            <td width="21" valign="top">A.</td>
            <td width="21" valign="top">1.</td>
            <td colspan="4">Berita Acara Pemungutan Suara dan Penghitungan Suara Pemilihan Umum Kepala Daerah                    dan Wakil Kepala Daerah di Tempat Pemungutan Suara (Model C – KWK.KPU) beserta                    lampiran :</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="28" align="center" valign="top">a.</td>
            <td colspan="3">Catatan Pelaksanaan Penghitungan Suara Pemilihan Umum Kepala Daerah dan Wakil                    Kepala Daerah di Tempat Pemungutan Suara (Model C 1 – KWK.KPU);</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="center" valign="top">b.</td>
            <td colspan="3">Sertifikat Hasil Penghitungan Suara Untuk Tiap Pasangan Calon Kepala Daerah dan                    Wakil Kepala Daerah di TPS (Lampiran Model C 1 – KWK.KPU);</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="center" valign="top">c.</td>
            <td colspan="3">Catatan Hasil Perolehan Suara Untuk Tiap Pasangan Calon Kepala Daerah dan Wakil                    Kepala Daerah di Tempat Pemungutan Suara (Model C 2 – KWK.KPU) ukuran besar;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="center" valign="top">d.</td>
            <td colspan="3">Pernyataan Keberatan Saksi dan Kejadian Khusus Yang Berhubungan Dengan Hasil                    Pemungutan Suara dan Penghitungan Suara Pemilihan Umum Kepala Daerah dan                    Wakil Kepala Daerah di Tempat Pemungutan Suara (Model C 3 – KWK.KPU);</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="center" valign="top">e.</td>
            <td colspan="3">Catatan Pembukaan Kotak Suara, Pengeluaran Isi, Identifikasi Jenis Dokumen, dan                    Penghitungan Jumlah Setiap Jenis Dokumen Untuk Pelaksanaan Pemungutan Suara                    Pemilihan Umum Kepala Daerah dan Wakil Kepala Daerah di Tempat Pemungutan                    Suara (Model C 4 – KWK.KPU);</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="center" valign="top">f.</td>
            <td colspan="3">Penggunaan Surat Suara Tambahan Yang Digunakan Sebagai Cadangan di Tempat                    Pemungutan Suara (Model C 5 – KWK.KPU);</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>2.</td>
            <td colspan="4">Seluruh surat suara (terpakai, tidak terpakai, keliru dicoblos dan rusak)</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>3.</td>
            <td colspan="4">Surat Pemberitahuan Waktu dan Tempat Pemungutan Suara (Model C 6 – KWK.KPU)</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>4.</td>
            <td colspan="4">Surat Pernyataan Pendamping Pemilih (Model C 7 – KWK.KPU)</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>5.</td>
            <td colspan="4">Daftar Nama Pemilih dari TPS Lain (Model C 8 – KWK.KPU)</td>
          </tr>
          <tr>
            <td height="42">B.</td>
            <td colspan="5">Alat kelengkapan TPS dan Berita Acara sebagaimana dimaksud pada huruf A dimasukkan ke                    dalam kotak suara.</td>
          </tr>
        </table>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <div align="right">
          <input name="textfield46" type="text" id="textfield46" size="10" />
          ,
  <input name="textfield4" type="text" id="textfield4" size="15" />
          20
  <input name="textfield48" type="text" id="textfield48" size="2" />
        </div>
        <table width="252" border="0" align="right">
          <tr>
            <td width="246" height="106" valign="top"><div align="center">
                <p>YANG MENERIMA<br />
                  PANITIA PEMUNGUTAN SUARA,</p>
            </div></td>
          </tr>
          <tr>
            <td><div align="center">(
                <input name="textfield6" type="text" id="textfield6" size="30" />
) NAMA JELAS</div></td>
          </tr>
        </table>
        <table width="252" border="0" align="left">
          <tr>
            <td width="246" height="106" valign="top"><div align="center">
                <p>YANG MENYERAHKAN<br />
                  KELOMPOK PENYELENGGARA<br />
                  PEMUNGUTAN SUARA</p>
            </div></td>
          </tr>
          <tr>
            <td><div align="center">(
              <input name="textfield5" type="text" id="textfield5" size="30" />
            ) NAMA JELAS</div></td>
          </tr>
        </table>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="left"><u>Catatan :</u><br />
          1. Lembar 1 untuk PPS;<br />
          2. Lembar 2 untuk KPPS.</p>
        <p align="left">&nbsp;</p>
        <table width="300" border="0" align="center">
          
        </table>
        </td>
    </tr>
  </table>
</form>