<form id="form1" name="form1" method="post" action="">
  <table width="626" border="0" align="center">
    <tr>
      <td><p align="right"><img src="image/kopsurat/c4.png" width="370" height="138" /></p>
        <p align="center">CATATAN PEMBUKAAN KOTAK SUARA,<br />
          PENGELUARAN ISI, IDENTIFIKASI JENIS DOKUMEN,<br />
          DAN PENGHITUNGAN JUMLAH SETIAP JENIS DOKUMEN UNTUK<br />
          PELAKSANAAN PEMUNGUTAN SUARA PEMILIHAN UMUM<br />
          KEPALA DAERAH DAN WAKIL KEPALA DAERAH<br />
        DI TEMPAT PEMUNGUTAN SUARA</p>
        <table width="592" border="0" bordercolor="#FFFFFF">
          <tr>
            <td width="207" valign="top">Pemilihan Umum</td>
            <td width="8" valign="top">:</td>
            <td width="355">Gubernur dan Wakil Gubernur /Bupati dan Wakil <br />
            Bupati/Walikota dan Wakil Walikota *) </td>
          </tr>
          <tr>
            <td>Tempat Pemungutan Suara (TPS) </td>
            <td>:</td>
            <td><input name="textfield78" type="text" id="textfield119" size="50" /></td>
          </tr>
          <tr>
            <td>Desa/Kelurahan *)</td>
            <td>:</td>
            <td><input name="textfield78" type="text" id="textfield120" size="50" /></td>
          </tr>
          <tr>
            <td>Kecamatan</td>
            <td>:</td>
            <td><input name="textfield78" type="text" id="textfield121" size="50" /></td>
          </tr>
          <tr>
            <td>Kabupaten/Kota *)</td>
            <td>:</td>
            <td><input name="textfield78" type="text" id="textfield122" size="50" /></td>
          </tr>
          <tr>
            <td>Provinsi</td>
            <td>:</td>
            <td><input name="textfield78" type="text" id="textfield123" size="50" /></td>
          </tr>
        </table>
        <p>Kelengkapan administrasi untuk pemungutan suara dan penghitungan suara yang diterima                dari PPS :</p>
        <table width="652" border="1" align="center">
          <tr>
            <td width="56" align="center">No.</td>
            <td width="440" align="center">URAIAN</td>
            <td width="134" align="center">KETERANGAN</td>
          </tr>
          <tr>
            <td align="center">1.</td>
            <td>Surat suara Pemilu Kepala Daerah dan Wakil Kepala Daerah</td>
            <td><input name="textfield" type="text" id="textfield" size="6" /> 
            lembar</td>
          </tr>
          <tr>
            <td align="center">2.</td>
            <td>Formulir Seri C Model C 1 – KWK.KPU s/d Model C 9 –<br />
              KWK.KPU (kecuali Model C 6 – KWK.KPU)</td>
            <td><input name="textfield2" type="text" id="textfield2" size="6" /> 
            set</td>
          </tr>
          <tr>
            <td align="center">3.</td>
            <td>Sampul</td>
            <td><input name="textfield3" type="text" id="textfield3" size="6" /> 
            lembar</td>
          </tr>
          <tr>
            <td align="center">4.</td>
            <td>Alat pemilihan</td>
            <td><input name="textfield4" type="text" id="textfield4" size="6" /> 
            set</td>
          </tr>
          <tr>
            <td align="center">5.</td>
            <td>Segel Pemilihan Umum</td>
            <td><input name="textfield5" type="text" id="textfield5" size="6" /> 
            lembar</td>
          </tr>
          <tr>
            <td align="center">6.</td>
            <td>Lem/perekat</td>
            <td><input name="textfield6" type="text" id="textfield6" size="6" /> 
            buah</td>
          </tr>
          <tr>
            <td align="center">7.</td>
            <td>Kantong Plastik</td>
            <td><input name="textfield7" type="text" id="textfield7" size="6" /> 
            buah</td>
          </tr>
          <tr>
            <td align="center">8.</td>
            <td>Karet pengikat surat suara</td>
            <td><input name="textfield8" type="text" id="textfield8" size="6" /> 
            buah</td>
          </tr>
          <tr>
            <td align="center">9.</td>
            <td>Spidol</td>
            <td><input name="textfield9" type="text" id="textfield9" size="6" /> 
            buah</td>
          </tr>
          <tr>
            <td align="center">10.</td>
            <td>Tanda khusus/tinta</td>
            <td><input name="textfield10" type="text" id="textfield10" size="6" /> 
            buah</td>
          </tr>
          <tr>
            <td align="center">11.</td>
            <td>Ballpoint selain warna hitam</td>
            <td><input name="textfield11" type="text" id="textfield11" size="6" /> 
            buah</td>
          </tr>
        </table>
        <p>&nbsp;</p>
        <div align="right">
          <input name="textfield46" type="text" id="textfield46" size="10" />
          ,
  <input name="textfield47" type="text" id="textfield47" size="15" />
          20
  <input name="textfield48" type="text" id="textfield48" size="2" />
        </div>
        <table width="252" border="0" align="right">
          <tr>
            <td width="242" align="center"><p>KELOMPOK PENYELENGGARA<br />
              PEMUNGUTAN SUARA<br />
              KETUA,</p>
                <p>&nbsp;</p>
            <p>&nbsp;</p></td>
          </tr>
          <tr>
            <td align="center">(
              <input name="textfield12" type="text" id="textfield12" size="30" />
            )</td>
          </tr>
        </table>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p align="left">*) Coret yang tidak perlu</p>
        <br align="left">
        <table width="300" border="0" align="center" bordercolor="#FFFFFF" bgcolor="#FFFFFF">
        
        </table>
      <p></p></td>
    </tr>
  </table>
</form>